#include <iostream>
#include <Python.h>

// load external api in a pointer
void * apiLoad(void * extApi, const char * apiName)
{
  // get python path
  wchar_t * pythonPath = Py_GetPath();
  std::wcout << "Python search path: " << pythonPath << std::endl;

  // init python
  Py_Initialize();
  fprintf(stderr, "Initialized Python\n");
  
  PyObject * pModule = PyImport_ImportModule(apiName);
  if (pModule == NULL)
  {
    fprintf(stderr, "Module not found\n");
    return NULL;
  }

  // load dictionary
  PyObject * pDict = PyModule_GetDict(pModule);
  fprintf(stderr, "Module address:\t %p\n", pDict);

  // assign address
  return (void *)pDict;
}

// call a function of the external api
double apiCallFunction(void * extApi, const char * func)
{ 
  PyObject * pDict = (PyObject *) extApi;
  
  // get function
  PyObject * pFunc = PyDict_GetItemString(pDict, func);
  fprintf(stderr, "%s pointer returned:\t %p\n", func, pFunc);
  if(pFunc == NULL)
  {
    fprintf(stderr, "%s called was not found in module.\n", func);
    return -1;
  }
  
  // call the function
  PyObject * pResult = PyObject_CallObject(pFunc, NULL);
  fprintf(stderr, "Pointer of Python result:\t %p\n", pResult);
  if(pResult == NULL)
  {
    fprintf(stderr, "Result is Null\n");
    return -1;
  }
  
  // check if PyObject is None
  if (pResult == Py_None)
  {
    fprintf(stderr, "Result is a None python object\n");
    return -1;
  }

  // convert PyObject to C value
  double result = PyLong_AsDouble(pResult);
  return result;
}

// unload external api
void apiUnload(void * extApi)
{
  
  // must be called at the end 
  Py_Finalize();
  fprintf(stderr, "Module unloaded:\t %p\n", extApi);
}
