// load external api in a pointer
void * apiLoad(void * extApi, const char * apiName);

// call a function of the external api
double apiCallFunction(void * extApi, const char * func);

// unload external api
void apiUnload(void * extApi);

