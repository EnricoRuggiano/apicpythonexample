# Python Path (Linux)
python interpreter will look for python module on:

``/usr/lib64/python3.x/```

# DaVinciResolve

1. copy ``DaVinciResolveScript.py`` on ``/usr/lib64/python3.x/``
2. copy ``python_get_resolve.py`` on ``/usr/lib64/python3.x/``

or

1. ``export PYTHONPATH=/usr/lib64/python3.x:/opt/resolve/Developer/Scripting/Modules/:/opt/resolve/Developer/Scripting/Examples/``

