#!/usr/bin/env python3

"""
External python API module
"""

# define a variable
variable = 10;

# some methods
def py_api_hello():
    print ("Hello from API!")

def py_api_print():
    print (variable)

def py_api_get_variable():
    return variable

def py_api_sum(add1, add2):
    return add1 + add2
