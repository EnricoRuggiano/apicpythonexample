#include "string"
#include "extAPI.h"

// global pointer to external API
void * extApi;

// emtry point
int main()
{
  // check address of external Api
  fprintf(stderr, "Init-external api address:\t %p\n", extApi);

  // load external api
  extApi = apiLoad(extApi, "python_get_resolve");  
  if (extApi == NULL)
  {
    return 0;
  }
  
  // check address of external Api
  fprintf(stderr, "After Load - external api address:\t %p\n", extApi);
 
  // print a result - call not present function
  double foo = apiCallFunction(extApi, "");
  fprintf(stderr, "Value from API:\t %f\n", foo);

  // print a result - call a present function
  const char * funcName = "GetResolve";
  double result = apiCallFunction(extApi, funcName);
  fprintf(stderr, "Value from API:\t %f\n", result);

  // unload external api
  apiUnload(extApi);

  // check address of external Api
  fprintf(stderr, "After Unload - external api address:\t %p\n", extApi);
 
  return 0;
}
