#include <Python.h>
#include <string>

int daVinciApi(std::string & resolveScriptPath, std::string & resolveFunctionName)
{

  Py_Initialize();
  PyObject* main = PyImport_AddModule("__main__");
  PyObject* gDictionary = PyModule_GetDict(main);
  PyObject* lDictionary = PyDict_New();

  std::string resolveScript =
    "import sys\n"
    "sys.path.append(" + resolveScriptPath + ")\n"
    "import DaVinciResolveNexGuardOfx as dv\n"
    "result = dv." + resolveFunctionName + "()\n"
    "print(result)\n";

  PyRun_String(resolveScript.c_str(), Py_file_input, gDictionary, lDictionary);
  int result = PyObject_IsTrue(PyDict_GetItemString(lDictionary, "result"));
  Py_Finalize();

  return result;
}

int main()
{

  // python script
  std::string resolveScriptPath = "'..'";
  std::string resolveFunctionName = "is_rendering";
   
  int result = daVinciApi(resolveScriptPath, resolveFunctionName);
  fprintf(stderr, "C++ Code: DaVinciApi %s:\t %d\n", resolveFunctionName.c_str(), result);

  return 0;
}