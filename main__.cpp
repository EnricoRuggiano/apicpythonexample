#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>

std::string exec(const char* cmd) 
{
    std::array<char, 1024> buffer;
    std::string result;

#if defined(_WIN32)
    std::unique_ptr<FILE, decltype(&_pclose)> pipe(_popen(cmd, "r"), _pclose);
# else
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
#endif
    if (!pipe) 
    {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

int daVinciApi(std::string & resolveScriptPath, std::string & resolveFunctionName)
{

  std::string pyScript = std::string("python ") + " \"" + resolveScriptPath + "\" " + resolveFunctionName;
  std::string daVinciResult= exec(pyScript.c_str());
  
  if (strcmp(daVinciResult.c_str(), "True\n") == 0)
  {
    return 1;
  }
  else if (strcmp(daVinciResult.c_str(), "False\n") == 0)
  {
    return 0;
  }
  else
  {
    return -1;
  }
}


int main()
{
    std::string pyScriptPath = "C:\\Program Files\\Common Files\\OFX\\Plugins\\NexGuardPlugin.ofx.bundle\\DaVinciResolveNexGuardOfx.py";
    std::string isRendering = "is_rendering";
    std::string checkOneClip = "check_one_clip";

    // script one
    int result = daVinciApi(pyScriptPath, isRendering);
    fprintf(stderr, "C++ Code: DaVinciApi:\t %s %d\n", isRendering.c_str(), result);

    // script two
    result = daVinciApi(pyScriptPath, checkOneClip);
    fprintf(stderr, "C++ Code: DaVinciApi:\t %s %d\n", checkOneClip.c_str(), result);
    return 0;
}